package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"runtime"
	"time"
)

// START OMIT
func main() {
	printMem()
	for i := 1; i < 25; i++ {
		go processConnection(i)
	}
	time.Sleep(3 * time.Second)
	printMem()
}

func processConnection(i int) {
	client := http.Client{Timeout: 10 * time.Second}
	response, err := client.Get(fmt.Sprintf("http://127.0.0.1:3999/?#q=%d", i))
	if err != nil {
		fmt.Println(err.Error())
	}
	defer response.Body.Close()
	ioutil.ReadAll(response.Body)
}

func printMem() {
	var mem runtime.MemStats
	runtime.ReadMemStats(&mem)
	fmt.Printf("StackInuse: %d\n", mem.StackInuse)
}

// END OMIT
