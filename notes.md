Before we begin
- Explain session contents
- Ask: Who has experience with Go
- Ask: Who has encountered a weird gotcha while writing code in Go?


#1
Problem: Variable scope of x is incorrect
Solution:
- Line 9, use = instead of :=
- Line 9, instantiate err as new var


#2
Problem: Start method is implemented on struct value instead of pointer
Solution:
- Line 18, add pointer (*) to FireStarter
- Line 23, add addressof (&) to FireStarter


#3
Problem: Client is created for each http request
Solution:
- Line 22, move to line 14
- Line 21, add client *http.Client as parameter
- Line 15, add client to method call


#4
Problem: go-routine closure is using the value of the iterator
Solution:
- Line 21, add i as parameter
- Line 25, include i in method call


#5
Problem: Data race, names[] is being assigned inside a go-routine
Solution: See next slide


#5b
Problem: Mutex is passed by value instead of using a pointer
Solution:
- Line 26, add pointer to sync.Mutex
- Line 16: add addressof to mut
