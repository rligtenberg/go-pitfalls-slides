package main

import "runtime"

// START OMIT
var m = make(map[int]int)

func main() {
	panicIfMoreThanOneCPUAvailable()
	c := make([]<-chan int, 0, 10)
	for i := 0; i < 10; i++ {
		c = append(c, getWorker())
	}
	for i := 0; i < 10; i++ {
		<-c[i]
	}
}

func getWorker() <-chan int {
	c := make(chan int)
	go func() {
		defer func() {
			c <- 1
		}()
		for i := 0; i < 1000000; i++ {
			m[5] = 6
		}
	}()
	return c
}

// END OMIT

func panicIfMoreThanOneCPUAvailable() {
	if runtime.NumCPU() <= 1 { // This is only here to make sure we run on > 1 CPU.
		panic("Only one CPU available")
	}
}
