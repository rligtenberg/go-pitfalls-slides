package main

import "fmt"

func main() {
	x := 1
	doneChan := make(chan bool)
	go func() {
		x, err := increase(x)

		if err != nil {
			fmt.Printf("Tried to increase %d, got error: %v\n", x, err.Error())
		}
		doneChan <- true
	}()
	<-doneChan
	fmt.Printf("x is %d", x)
}

func increase(x int) (int, error) {
	x++ // "heavy" calculation
	return x, nil
}
