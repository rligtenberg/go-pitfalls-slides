# Go - Pitfalls you want to avoid
This repo contains the slide deck and the required source files from the knowledge sharing session. 

# Prerequisite: Install presentation tool
The slide deck requires you to install `present`, a Go tool for running the slide deck in your browser. Instructions for installing:
 
1. Run this command: `go install golang.org/x/tools/cmd/present`
2. Ensure that the path `%GOPATH%/bin` is part of your `PATH` env var.
3. Open the root of this repo in a terminal.
4. Run `present`
5. Open the url in your browser.
  
More information about `present`: 
* https://godoc.org/golang.org/x/tools/present
* https://godoc.org/golang.org/x/tools/cmd/present

# Run the presentation

When you run present and open the repo homepage, it will show you a link to the slide deck (go-pitfalls.slide). If you run `present -notes`, you can open the synchronized version of the slide deck that includes the notes describing the problem and solution.


Enjoy!



![Professor image](gopher-professor.jpg)
