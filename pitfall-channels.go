package main

import (
	"fmt"
	"math/rand"
	"time"
)

type person struct {
	name string
	age  int
}

// START OMIT
func main() {
	rand.Seed(time.Now().UnixNano())
	personCreated := make(chan person, 10)

	for i := 0; i < 10; i++ {
		go func() {
			time.Sleep(time.Duration(rand.Intn(100)) * time.Millisecond)
			person := person{name: fmt.Sprintf("Person %d", i), age: rand.Intn(50) + 20}
			personCreated <- person
		}()
	}

	for i := 0; i < 10; i++ {
		select {
		case p := <-personCreated:
			fmt.Printf("%v (age %d) is ready\n", p.name, p.age)
		}
	}
	fmt.Println("Done!")
}

// END OMIT
