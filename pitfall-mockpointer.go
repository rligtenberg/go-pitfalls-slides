package main

import "fmt"

// START OMIT
type Starter interface {
	Start()
}

type FireStarter struct {
	isStarted bool
}

type Engine struct {
	Starter Starter
}

func (f FireStarter) Start() {
	f.isStarted = true
}

func main() {
	f := FireStarter{}
	e := Engine{Starter: f}
	e.Starter.Start()
	fmt.Printf("Is started: %v", f.isStarted)
}

// END OMIT
