package main

import (
	"fmt"
	"strings"
	"sync"
	"time"
)

// START OMIT
func main() {
	var names = []string{"John Wick", "Paul Newman", "Ben Kenobi"}
	var mut = sync.Mutex{}

	for i := range names {
		go updateName(i, names, mut)
	}

	time.Sleep(100 * time.Millisecond)

	mut.Lock()
	defer mut.Unlock()
	fmt.Println(names)
}

func updateName(i int, names []string, mut sync.Mutex) {
	mut.Lock()
	defer mut.Unlock()
	parts := strings.Split(names[i], " ")
	names[i] = parts[1] + ", " + parts[0]
}

// END OMIT
