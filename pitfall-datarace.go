package main

import (
	"fmt"
	"strings"
	"time"
)

// START OMIT
func main() {
	var names = []string{"John Wick", "Paul Newman", "Ben Kenobi"}

	for i := range names {
		go updateName(i, names)
	}

	time.Sleep(100 * time.Millisecond)

	fmt.Println(names)
}

func updateName(i int, names []string) {
	parts := strings.Split(names[i], " ")
	names[i] = parts[1] + ", " + parts[0]
}

// END OMIT
